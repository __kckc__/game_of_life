//============================================================================
// Name        : main.cpp
// Author      : Kenneth Chan
// Version     : 1.0.0
// Copyright   : KC
// Description : Game of Life, invented by John Horton Conway
// Source      : http://en.wikipedia.org/wiki/Conway's_Game_of_Life
//============================================================================

#include <iostream>
#include <cstdlib>
#include <fstream>
#include "game_of_life.h"

int main(int argc, char* argv[]) {

	//program requires generation spec and filename of initial matrix
	if ( argc != 3 )
		//do not continue if otherwise
		std::cout << "usage: "<< argv[0]
		          << " <# of generations> <filename of initial matrix>\n";
	else
	{
		int i_gen = atoi(argv[1]); //number of generations
		std::ifstream i_file(argv[2]); //file handler to the initial matrix

		//do not continue if generations is not a positive number
		if (i_gen <= 0)
			std::cout << "error: <generations> needs to be greater than zero \n";
		//do not continue if input file does not exists
		else if (!(i_file))
			std::cout << "error: "<< argv[2] << " does not exists \n";
		else
		{
			char world[ROWS][COLUMNS]; //matrix of the world

			//load input file data into matrix
			load_matrix(world, i_file);
			//close file after loading
			i_file.close();

			//iterate through generations to arrive at the final matrix
			for (int i = 1; i <= i_gen; i++)
				generation(world);

			//display matrix
			print_matrix(world);
		}
	}

	return 0;
}
