//============================================================================
// Name        : game_of_life.h
// Author      : Kenneth Chan
// Version     : 1.0.0
// Copyright   : KC
// Description : header file for game of life functions
//============================================================================

#ifndef GAMEOFLIFE_H_
#define GAMEOFLIFE_H_

//can modify in the future if matrix size changes
const int ROWS = 40; //number of rows in matrix
const int COLUMNS = 80; //number of columns in matrix

//load matrix from input file
void load_matrix(char world[][COLUMNS], std::ifstream& i_file);
//Precondition: an empty multi-dimensional array called world
//Postcondition: array populated based on input file

//print matrix to output
void print_matrix(const char world[][COLUMNS]);

//conducts the birth or death of every cell in the matrix
void generation(char world[][COLUMNS]);

//count the neighbors of any cell
int neighbors(const char world[][COLUMNS], int r, int c);

#endif /* GAMEOFLIFE_H_ */
