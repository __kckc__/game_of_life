//============================================================================
// Name        : game_of_life.cpp
// Author      : Kenneth Chan
// Version     : 1.0.0
// Copyright   : KC
// Description : source file for game of life functions
//============================================================================

#include <iostream>
#include <fstream>
#include "game_of_life.h"

//loads input file into matrix
void load_matrix(char world[][COLUMNS], std::ifstream& i_file)
{
	std::string s_line; //stores single line of data
	int i_row = -1; //row index, will start at zero on first read
	int i_col; //column index

	//initialize all cells to ' '
	for(int r = 0; r < ROWS; r++)
		for(int c = 0; c < COLUMNS; c++)
			world[r][c] = ' ';

	//loop through each line of data in file
	while (std::getline(i_file, s_line))
	{
		i_row++; //increment row index
		i_col = 0; //reset column index to zero on new row

		//loop to store each char from row
		//stop if row is larger than matrix column size
		while ((i_col < COLUMNS) && (i_col < s_line.size()))
		{
			world[i_row][i_col] = s_line[i_col]; //store char
			i_col++; //increment column index
		}

		//test output
		//std::cout << "size: " << s_line.size() << std::endl;
	}

	return;
}

//print matrix to output
void print_matrix(const char world[][COLUMNS])
{
	//loop through each row and column to display to output
	for(int r = 0; r < ROWS; r++)
	{
		for (int c = 0; c < COLUMNS; c++)
			std::cout << world[r][c];
		std::cout << std::endl; //line break after each row
	}

	return;
}

//conducts the birth or death of every cell in the matrix
void generation(char world[][COLUMNS])
{
	int neighbor_count; //keeps track of neighbors for each cell

	/* loop through each cell and determine its fate
	 * ' ' denotes dead cell
	 * '*' denotes living cell
	 * '.' denotes dying cell
	 * 'o' denotes new born cell
	 */
	for(int r = 0; r < ROWS; r++)
		for (int c = 0; c < COLUMNS; c++)
		{
			//count up all the neighbors
			neighbor_count = neighbors(world, r, c);

			//test output to check count
			//if (neighbor_count)
			//	std::cout << "(" << r << "," << c << "): "
			//			  << neighbor_count << std::endl;

			//kill cell if it has less than 2 or more than 3 neighbors
			if ((world[r][c] == '*') && ((neighbor_count < 2) || (neighbor_count > 3)))
				world[r][c] = '.';

			//give birth to cell if it has exactly 3 neighbors
			if ((world[r][c] == ' ') && (neighbor_count == 3))
				world[r][c] = 'o';
		}

	/* loop through each cell and commit its fate
	 * '.' --> ' '
	 * 'o' --> '*'
	 */
	for(int r = 0; r < ROWS; r++)
		for (int c = 0; c < COLUMNS; c++)
		{
			if (world[r][c] == '.')
				world[r][c] = ' '; //dead cell
			else if (world[r][c] == 'o')
				      world[r][c] = '*'; //living cell
		}

	return;
}

//count the neighbors of any cell
int neighbors(const char world[][COLUMNS], int r, int c)
{
	int neighbor_count = 0; //neighbor counter

	//loop through a 3x3 mini-matrix to count neighbors
	for(int j = r - 1; j <= r + 1; j++)
		for (int k = c - 1; k <= c + 1; k++)
		{
			//if in range
			if ((j >= 0) && (j < ROWS) && (k >= 0) && (k < COLUMNS))
				//and if coordinates is not the cell itself
				if (!((j == r) && (k == c)))
					//and if cell is dying '.' or living '*'
					if ((world[j][k] == '.') || (world[j][k] == '*'))
						//then increment counter
						neighbor_count++;
		}

	return neighbor_count;
}
