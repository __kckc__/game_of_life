'''

description
----------
- project: game of life
- purpose: for red so team
- author: kc
- creation date: 18-nov-2013
- last modification date: 24-dec-2013

notes
----------
- before compiling, ensure that you have g++ installed
- to **compile**, type 'make'
- to **run**, i.e., type './run_game 10 initial_grid.txt'
- to **clear binaries**, type 'make clean'

'''
